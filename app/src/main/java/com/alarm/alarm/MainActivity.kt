package com.alarm.alarm

import android.content.Intent
import android.os.Bundle
import com.alarm.alarm.Activities.Splash.SplashActivity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity

/**Todas las Activitys tienen que heredad de BaseActivity()
 * En el metodo onCreate ya no pasas la vista en el set contentView, ahora lo haces con el metodo:
 * "override fun setContentView(): Int = R.layout.nombre_de_la_vista"
 * */

class MainActivity : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, SplashActivity::class.java)
        startActivity(intent)
    }


}
