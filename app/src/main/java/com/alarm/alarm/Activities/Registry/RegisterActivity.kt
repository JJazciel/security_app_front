package com.alarm.alarm.Activities.Registry

import android.content.Intent
import android.os.Bundle
import com.alarm.alarm.Activities.Enroll.LoginActivity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.Data.Models.ModelsBody.RegisterBody
import com.alarm.alarm.R
import com.alarm.alarm.Utils.RXUtils
import com.alarm.alarm.Utils.toast
import kotlinx.android.synthetic.main.activity_registry.*


class RegisterActivity : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_registry
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setEvents()
    }

    private fun setEvents() {
        btnRegistry.setOnClickListener {
            registerUser()
        }

        btnSendToRegistry.setOnClickListener {
            //TODO(Esto deberia estar en un metodo)
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    //TODO(Hay que ser estupidamente especificos con el nombre de los metodos, y usar nomenclaturas hara mas facil leer el codigo)
    private fun registerUser() {
        if (isValidData(getData())) {
            compositeDisposable.add(
                retrofitModule.registerUser(getData())
                    .compose(RXUtils.netWork())
                    .subscribe({
                        if (it.response) {
                            goToLoginActivity()
                        } else {
                            if (it.message.isNotBlank()) {
                                toast(it.message)
                            } else if (it.errors.isNotBlank()) {
                                toast(it.errors)
                            }
                        }
                    }, {
                        toast(it.message.toString())
                    })
            )
        }
    }

    private fun getData(): RegisterBody {
        //TODO(El codigo de pais, y tipo de usuario deberian de ser constantes en vez de estar hardcodeadas, para eso existe el archivo de Constantes)
        return RegisterBody(
            edtTName.text.toString(),
            edtTLastName.text.toString(),
            "52",
            edtTPhone.text.toString(),
            edtTPassword.text.toString(),
            "2"
        )
    }

    private fun isValidData(data: RegisterBody): Boolean {
        var isValid = false

        if (data.Nombre.isBlank()) {
            toast("Llena el campo nombre.")
        } else isValid = true

        if (data.Apellidos.isBlank()) {
            toast("Llena el campo Apellidos.")
        } else isValid = true

        if (data.Telefono.isBlank()) {
            toast("Llena el campo teléfono.")
        } else isValid = true

        if (data.Password.isBlank()) {
            toast("Llena el campo contraseña.")
        } else isValid = true

        return isValid
    }

    private fun goToLoginActivity() {
        startActivity(
            Intent(
                this@RegisterActivity,
                LoginActivity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
        finish()
    }
}
