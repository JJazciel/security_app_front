            package com.alarm.alarm.Activities.Menu

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.util.LogPrinter
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.text.isDigitsOnly
import androidx.core.view.GravityCompat
import com.alarm.alarm.Activities.ConfigurationProfile.ProfileConfiguration
import com.alarm.alarm.Activities.Contact.ContactActivity
import com.alarm.alarm.Activities.Maps.AlertMapsActivity
import com.alarm.alarm.Activities.Profile.ChangeImageProfile.Change_Image_Profile_Activity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.Data.Models.ModelsBody.AlertBody
import com.alarm.alarm.Data.Models.ModelsBody.Direccion
import com.alarm.alarm.R
import com.alarm.alarm.Utils.Constants
import com.alarm.alarm.Utils.RXUtils
import com.alarm.alarm.Utils.toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.content_menu_.*
import kotlinx.io.IOException
import java.util.*
import java.util.jar.Manifest

class MenuActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location //Última localización

    override fun getLayoutResource(): Int = R.layout.activity_menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setEvents()
        chargePreferences()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    private fun setUpMap(tipoAlerta: Int, subtipoAlerta: Int) {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                Constants.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        fusedLocationClient.lastLocation.addOnSuccessListener(this) {location ->
            if (location != null) {
                lastLocation = location
                val currentLatLong = LatLng(location.latitude, location.longitude)
                getAddress(currentLatLong, tipoAlerta, subtipoAlerta)
            }
        }
    }

    private fun getAddress(latLng: LatLng, tipoAlerta: Int, subtipoAlerta: Int) {
        val geocoder = Geocoder(this, Locale.getDefault())
        val addresses: List<Address>?
        val street: String?
        val outdoor: String?
        val suburb: String?
        val municipality: String?
        val city: String?
        val state: String?

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            Log.w("DireccionGeocorer", addresses.toString())
            if (addresses != null && !addresses.isEmpty()) {
                if (addresses[0].thoroughfare != null)
                    street = addresses[0].thoroughfare
                else
                    street = ""

                if (addresses[0].featureName != null)
                    outdoor = addresses[0].featureName
                else
                    outdoor = ""

                if (addresses[0].subLocality != null)
                    suburb = addresses[0].subLocality
                else
                    suburb = ""

                if (addresses[0].subAdminArea != null)
                    municipality = addresses[0].subAdminArea
                else
                    municipality = ""

                if (addresses[0].locality != null)
                    city = addresses[0].locality
                else
                    city = ""

                if (addresses[0].adminArea != null)
                    state = addresses[0].adminArea
                else
                    state = ""

                Log.w("Municipio", municipality)

                compositeDisposable.add(retrofitModule.issueAlert( AlertBody(
                    prefs.idUser.toString(),
                    tipoAlerta,
                    subtipoAlerta,
                    Direccion(
                        street.toString(),
                        outdoor.toString(),
                        suburb.toString(),
                        municipality.toString(),
                        city.toString(),
                        state.toString(),
                        latLng.latitude,
                        latLng.longitude,
                        1
                    )
                ) )
                    .compose(RXUtils.netWork())
                    .subscribe({
                        if (it.response) {
                            toast(it.result, Toast.LENGTH_LONG)
                        } else {
                            toast(it.errors, Toast.LENGTH_LONG)
                        }
                    }, {
                        toast(it.message.toString())
                    })
                )
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }
    }

    private fun setEvents() {
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        fabMenu.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
        }

        imgVStole.setOnLongClickListener {
            setUpMap(1, 1)
            return@setOnLongClickListener true
        }

        imgVAccident.setOnLongClickListener {
            setUpMap(2, 3)
            return@setOnLongClickListener true
        }

        imgVKidnap.setOnLongClickListener {
            setUpMap(1, 2)
            return@setOnLongClickListener true
        }

        imgVFamilyProblems.setOnLongClickListener {
            setUpMap(1, 4)
            return@setOnLongClickListener true
        }

        imgVHealthProblems.setOnLongClickListener {
            setUpMap(2, 5)
            return@setOnLongClickListener true
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            //R.id.itemGoToProfile -> {
            //    startActivity(Intent(this, Change_Image_Profile_Activity::class.java))
            //}

            R.id.itemGoToContact -> {
                showContactsView()
            }
            R.id.itemGoToTravel -> {
                toast("Go to itemGoToTravel")
            }
            R.id.itemGoToConfiguration -> {
                showConfigurationProfile()
            }
        }

        //close navigation drawer
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showContactsView() {
        val intent = Intent(this, ContactActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    private fun showConfigurationProfile(){
        val intent = Intent(this, ProfileConfiguration::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
    }

    @SuppressLint("SetTextI18n")
    private fun chargePreferences() {
        val navUserName =
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.nav_user_name)
        navUserName.text = prefs.nameUser + " " + prefs.lastNameUser
        txtViewNameUser.text = prefs.nameUser

        val ivProfilePhoto =
            nav_view.getHeaderView(0).findViewById<ImageView>(R.id.nav_user_image)
        ivProfilePhoto?.let {
            Picasso.get()
                .load(prefs.imageUser)
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder)
                .into(it)
        }
    }
}