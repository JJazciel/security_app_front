package com.alarm.alarm.Activities.Contact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alarm.alarm.Data.Models.ModelsResult.GetContactsResult
import com.alarm.alarm.R
import com.alarm.alarm.Utils.CallbackItem
import com.alarm.alarm.Utils.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactsAdapter(private var items: List<GetContactsResult> = emptyList(), private val callbackDelete: CallbackItem<GetContactsResult>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false))
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    fun setItems(items: List<GetContactsResult>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.bind(items[position], callbackDelete)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: GetContactsResult,
                 callbackDelete: CallbackItem<GetContactsResult>) =
            with(itemView) {
                Picasso.get()
                    .load(item.ImagenPerfil)
                    .placeholder(R.drawable.ic_account_placeholder)
                    .error(R.drawable.ic_account_placeholder)
                    .fit()
                    .into(this.item_contact_photo)
            this.item_contact_name.text = item.Nombre
            this.item_contact_parent.text = item.Parentezco
            this.item_contact_delete.setOnClickListener {
                callbackDelete.onClickItem(it, item)
            }
        }
    }
}