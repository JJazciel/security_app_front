package com.alarm.alarm.Activities.ConfigurationProfile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.alarm.alarm.Activities.Profile.ChangeImageProfile.Change_Image_Profile_Activity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.R
import com.alarm.alarm.Utils.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_profile_configuration.*

class ProfileConfiguration : BaseActivity() {
    override fun getLayoutResource(): Int = R.layout.activity_profile_configuration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()

        setevents()
    }

    private fun init(){
        chargePreferences()
    }

    private fun setevents() {
        btn_change_image_profile.setOnClickListener {
            startActivity(Intent(this, Change_Image_Profile_Activity::class.java))
        }

        btn_change_name_profile.setOnClickListener {
            toast("Cambiar nombre")
        }

        btn_edit_password.setOnClickListener {
            editPassword()
        }

        btn_edit_phone.setOnClickListener {
            editPhone()
        }

        btn_log_out.setOnClickListener {
            toast("Cerrar sesión")
        }
    }

    private fun chargePreferences(){
        setToolbar("Mi perfil")
        txtPhoneNumber.text = prefs.phoneUser
        txtNameUser.text = prefs.nameUser + " " + prefs.lastNameUser

        val ivProfilePhoto =
            linearLayout.findViewById<ImageView>(R.id.imv_image_profile)
        ivProfilePhoto?.let {
            Picasso.get()
                .load(prefs.imageUser)
                .placeholder(R.drawable.ic_user_placeholder_red)
                .error(R.drawable.ic_user_placeholder_red)
                .into(it)
        }
    }

    private fun editPassword(){
        toast("Edit password")
    }

    private fun editPhone(){
        toast("Edit phone")
    }
}