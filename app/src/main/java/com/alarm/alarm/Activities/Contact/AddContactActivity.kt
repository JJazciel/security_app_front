package com.alarm.alarm.Activities.Contact

import android.os.Bundle
import androidx.core.text.HtmlCompat
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.R
import kotlinx.android.synthetic.main.activity_add_contact.*

class AddContactActivity : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_add_contact
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        setToolbar(R.string.title_contacts)
        contacts_sub_title.text = HtmlCompat.fromHtml(
            getString(
                R.string.txt_limit_contact,
                prefs.remainingContacts.toString()
            ), HtmlCompat.FROM_HTML_MODE_COMPACT
        )
    }
}