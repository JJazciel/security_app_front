package com.alarm.alarm.Activities.Enroll

import android.content.Intent
import android.os.Bundle
import com.alarm.alarm.Activities.Menu.MenuActivity
import com.alarm.alarm.Activities.Registry.RegisterActivity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.Data.Models.ModelsBody.LoginBody
import com.alarm.alarm.Data.Models.ModelsBody.RegisterDeviceTokenBody
import com.alarm.alarm.Data.Models.ModelsResult.getDataResult
import com.alarm.alarm.R
import com.alarm.alarm.Utils.Constants.Companion.COUNTRY_CODE
import com.alarm.alarm.Utils.Constants.Companion.NORMAL_TYPE_USER
import com.alarm.alarm.Utils.RXUtils
import com.alarm.alarm.Utils.toast
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_login
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setEvents()
    }

    private fun setEvents() {
        button_Login.setOnClickListener {
            login()
        }

        btnSendToRegistry.setOnClickListener {
            showRegisterView()
        }

        btnForgotPassword.setOnClickListener {
            forgotPassword()
        }
    }

    private fun forgotPassword() {
        toast("Olvidé mi contraseña")
    }

    private fun showRegisterView(){
        val intent = Intent(this, RegisterActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun login() {
        if (isValidUserData(getDataFromView())) {
            compositeDisposable.add(retrofitModule.login(getDataFromView())
                    .compose(RXUtils.netWork())
                    .subscribe({
                        if (it.response) {
                            val token = it.result.token
                            getUserData(token)
                        } else {
                            toast(it.errors)
                        }
                    }, {
                        toast(it.message.toString())
                    })
            )
        }
    }

    /**TODO(Siempre hay que ser estupidamente especificos con los nombres de los metodos, variables, clases, paquetes, recursos, todo!!)*/
    fun getUserData(tokenUser: String) {
        compositeDisposable.add(retrofitModule.getUserData(tokenUser)
                .compose(RXUtils.netWork())
                .subscribe({
                    if (it.response) {
                        saveUserData(it.result)
                        getDeviceToken()
                        onUserLogged()
                    } else {
                        toast(it.message)
                    }
                }, {
                    //TODO(Todos los textos que pongas en la aplicacion, deben ser recursos de string, inluirlos en el xml values/strings.xml, es mejor que dejarlos hardcodeados)
                    toast(R.string.txt_error_conectando.toString())
                }))
    }

    private fun getDataFromView(): LoginBody {
        //TODO(El codigo de pais, y tipo de usuario deberian de ser constantes en vez de estar hardcodeadas, para eso existe el archivo de Constantes)
        return LoginBody(
            COUNTRY_CODE,
            edtTNameUser.text.toString(),
            edtTPassword.text.toString(),
            NORMAL_TYPE_USER
        )
    }

    private fun isValidUserData(data: LoginBody): Boolean {
        //TODO(Todos los textos que pongas en la aplicacion, deben ser recursos de string, inluirlos en el xml values/strings.xml, es mejor que dejarlos hardcodeados)
        var isValid = false

        if (data.Telefono.isBlank()) {
            toast("Llena el campo nombre de usuario")
        } else isValid = true

        if (data.Password.isBlank()) {
            toast("Llena el campo contraseña")
        } else isValid = true

        return isValid
    }

    private fun onUserLogged() {
        val intent = Intent(this@LoginActivity, MenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun getDeviceToken(){
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            it.result?.token?.let {
                prefs.deviceToken = it
                registerDeviceToken()
            }
        }
    }

    private fun getDataDeviceToken(): RegisterDeviceTokenBody{
        return RegisterDeviceTokenBody(
            prefs.idUser.toString(),
            "Android",
            prefs.deviceToken.toString()
        )
    }

    private fun registerDeviceToken(){
        compositeDisposable.add(
            retrofitModule.registerDeviceToken(getDataDeviceToken())
                .compose(RXUtils.netWork())
                .subscribe({
                    if (it.response){
                        println(it)
                    } else {
                        if (it.message.isNotBlank()) {
                            toast(it.message)
                        } else if (it.errors.isNotBlank()) {
                            toast(it.errors)
                        }
                    }
                }, {
                    toast(it.message.toString())
                })
        )
    }

    private fun saveUserData(result: getDataResult) {
        prefs.idUser = result.idUsuario
        prefs.nameUser = result.Nombre
        prefs.lastNameUser = result.Apellidos
        prefs.countryCodeUser = result.CodigoPais
        prefs.phoneUser = result.Telefono
        prefs.emailUser = result.Correo
        prefs.imageUser = result.ImagenUsuario
        prefs.typeUser = result.TipoUsuario
        prefs.isLogin = true
    }
}
