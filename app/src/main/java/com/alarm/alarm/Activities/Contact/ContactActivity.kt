package com.alarm.alarm.Activities.Contact

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.Data.Models.ModelsBody.ContactBody
import com.alarm.alarm.Data.Models.ModelsResult.GetContactsResult
import com.alarm.alarm.Data.Retrofit.Services.RetrofitModule
import com.alarm.alarm.R
import com.alarm.alarm.Utils.CallbackItem
import com.alarm.alarm.Utils.Constants.Companion.CONTACT_LIMIT
import com.alarm.alarm.Utils.Constants.Companion.COUNTRY_CODE
import com.alarm.alarm.Utils.Constants.Companion.MAX_CONTACTS
import com.alarm.alarm.Utils.Constants.Companion.PICK_CONTACT_REQUEST
import com.alarm.alarm.Utils.RXUtils
import com.alarm.alarm.Utils.toast
import kotlinx.android.synthetic.main.activity_contact.*
import java.net.URI

class ContactActivity : BaseActivity() {
    private var adapter: ContactsAdapter? = null
    private val PERMISSION_CONTACTS = 1000

    override fun getLayoutResource(): Int = R.layout.activity_contact
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    private fun init() {
        setToolbar(R.string.title_contacts)
        contacts_label_add.text = HtmlCompat.fromHtml(
            getString(R.string.txt_add_contact),
            HtmlCompat.FROM_HTML_MODE_COMPACT
        )
        hideAddContact()
        initAdapter()
        getContacts()
        setEvents()
    }

    private fun setEvents() {
        refresh.setOnRefreshListener {
            getContacts()
            refresh.isRefreshing = false
        }

        contacts_add_fab.setOnClickListener {
            goToAddContact()
        }
    }

    private fun initAdapter() {
        adapter = ContactsAdapter(emptyList(), getCallbackDelete())
        contacts_recycler.layoutManager = LinearLayoutManager(this)
        contacts_recycler.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_contacts, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_add_contact -> goToAddContact()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getContacts() {
        prefs.idUser?.let { idUser ->
            compositeDisposable.add(
                retrofitModule.getContacts(idUser)
                    .compose(RXUtils.netWork())
                    .subscribe({
                        if (it.response) {
                            prefs.remainingContacts = MAX_CONTACTS - it.result.size
                            prefs.totalContacts = it.result.size
                            adapter?.setItems(it.result)
                            hideAddContactLayout()
                            hideAddContact()
                        } else {
                            showAddContact()
                            showAddContactLayout()
                            if (it.message.isNotBlank()) {
                                toast(it.message)
                            } else if (it.errors.isNotBlank()) {
                                prefs.remainingContacts = MAX_CONTACTS
                                prefs.totalContacts = it.result.size
                                toast(it.errors)
                            }
                        }
                    }, {
                        hideAddContact()
                        toast(it.message.toString())
                    })
            )
        }
    }

    private fun showAddContactLayout() { //Vista de contactos
        contacts_layout_add.visibility = VISIBLE
    }

    private fun hideAddContactLayout() { //Vista de contactos
        contacts_layout_add.visibility = GONE
    }

    private fun showAddContact(){
        contacts_add_fab.visibility = VISIBLE
        contacts_label_add.visibility =  VISIBLE
    }

    private fun hideAddContact(){
        contacts_add_fab.visibility = GONE
        contacts_label_add.visibility =  GONE
    }

    //TODO: Agregar contactos
    private fun goToAddContact() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED) {
                val permission_contact = arrayOf(Manifest.permission.READ_CONTACTS)
                requestPermissions(permission_contact, PERMISSION_CONTACTS)
            } else
                if (prefs.totalContacts < MAX_CONTACTS) {
                    showContactPhoneView()
                }
        } else
            if (prefs.totalContacts < MAX_CONTACTS) {
                showContactPhoneView()
            }
    }

    private fun showContactPhoneView(){
        val intent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE)
        startActivityForResult(intent, PICK_CONTACT_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                val uri_data: Uri = data!!.data
                val cursor: Cursor = contentResolver.query(uri_data, null, null, null, null)

                if (cursor.moveToFirst()) {
                    val nameColumn: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                    val phoneColumn: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

                    val nameContact: String = cursor.getString(nameColumn)
                    val phoneContact: String = cursor.getString(phoneColumn)
                    val phoneContactFinal: String

                    phoneContactFinal = if (phoneContact.count() > 10) {
                        val phoneContactReplace: String = phoneContact.replace(" ", "")
                        val phoneCount: Int = phoneContactReplace.count()
                        phoneContactReplace.substring(phoneCount - CONTACT_LIMIT, phoneCount)
                    } else
                        phoneContact

                    compositeDisposable.add(retrofitModule.addContact(ContactBody(
                        nameContact,
                        COUNTRY_CODE,
                        phoneContactFinal,
                        "Amigo",
                        prefs.idUser.toString()
                    ))
                        .compose(RXUtils.netWork())
                        .subscribe({
                            if (it.response) {
                                toast(it.message)
                                getContacts()
                                refresh.isRefreshing = false
                            } else {
                                toast(it.errors)
                            }
                        }, {
                            toast(it.message.toString())
                        })
                    )
                }
            }
        }
    }

    //TODO: Eliminar contacto
    private fun getCallbackDelete(): CallbackItem<GetContactsResult> {
        return object : CallbackItem<GetContactsResult> {
            override fun onClickItem(view: View, item: GetContactsResult) {
                alertDialog(item.idContacto)
            }
        }
    }

    private fun alertDialog(idContact: Int){
        val builder_alert_dialog: AlertDialog.Builder = AlertDialog.Builder(this)
        builder_alert_dialog.setTitle(R.string.title_delete_dialog)
        builder_alert_dialog.setMessage(R.string.message_delete_dialog)
            .setPositiveButton(R.string.positive_dialog_button, DialogInterface.OnClickListener { dialog, which ->
                compositeDisposable.add(retrofitModule.deleteContact(idContact, prefs.idUser!!.toInt())
                    .compose(RXUtils.netWork())
                    .subscribe({
                        if (it.response) {
                            toast(it.message)
                            getContacts()
                            refresh.isRefreshing = false
                        } else {
                            toast(it.errors)
                        }
                    },{
                        toast(it.message.toString())
                    }))
            })
            .setNegativeButton(R.string.negative_dialog_button, DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })
            .setCancelable(false)
            .show()
    }
}
