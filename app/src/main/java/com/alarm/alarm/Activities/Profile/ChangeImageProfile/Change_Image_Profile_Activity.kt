package com.alarm.alarm.Activities.Profile.ChangeImageProfile

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.alarm.alarm.DaggerDependecyInjector.Base.BaseActivity
import com.alarm.alarm.R
import com.alarm.alarm.Utils.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_change__image__profile.*
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_profile.*


class Change_Image_Profile_Activity : BaseActivity() {

    private val PERMISSION_CAMERA = 1000
    private val PERMISSION_GALLERY = 1001
    var image_uri: Uri? = null

    override fun getLayoutResource(): Int = R.layout.activity_change__image__profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change__image__profile)

        chargeImage()

        btnGalery.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permission_gallery = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permission_gallery, PERMISSION_GALLERY)
                } else
                    openGalery()
            } else
                openGalery()
        }

        btnCamera.setOnClickListener {
            //if system os is Marshmallow or Above, we need to request runtime permission
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission was not enabled
                    val permission_camera = arrayOf(
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                    //show popup to request permission
                    requestPermissions(permission_camera, PERMISSION_CAMERA)
                } else
                    openCamera()
            } else
                openCamera()
        }
    }

    private fun openGalery() {
        toast("Open galery")
        val intent_gallery = Intent(Intent.ACTION_PICK)
        intent_gallery.type = "image/*"
        startActivityForResult(intent_gallery, PERMISSION_GALLERY)
    }

    private fun openCamera() {
        toast("Open camera")
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, PERMISSION_CAMERA)
    }

    private fun chargeImage() {
        //Picasso.get().load(url).into(imgViewProfile)
        val ivProfilePhoto = findViewById<ImageView>(R.id.imgViewChangeProfile)
        ivProfilePhoto?.let {
            Picasso.get()
                .load(prefs.imageUser)
                .placeholder(R.drawable.ic_user_placeholder_red)
                .error(R.drawable.ic_user_placeholder_red)
                .into(it)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when (requestCode) {
            PERMISSION_CAMERA -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openCamera()
                else
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
            }

            PERMISSION_GALLERY -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openGalery()
                else
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK && requestCode == PERMISSION_CAMERA)
            imgViewChangeProfile.setImageURI(image_uri)

        if (resultCode == Activity.RESULT_OK && requestCode == PERMISSION_GALLERY)
            imgViewChangeProfile.setImageURI(data?.data)
    }
}
