package com.alarm.alarm.DaggerDependecyInjector.module

import android.content.Context
import com.alarm.alarm.DaggerDependecyInjector.BaseApplication
import com.alarm.alarm.DaggerDependecyInjector.builder.ActivityBuilder
import com.alarm.alarm.DaggerDependecyInjector.builder.ServiceInjectors
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(
    includes = [
        NetworkModule::class,
        ActivityBuilder::class,
        ServiceInjectors::class
    ]
)
abstract class AppModule {

    @Singleton
    @Binds
    internal abstract fun provideContext(application: BaseApplication): Context

}