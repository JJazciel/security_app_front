package com.alarm.alarm.DaggerDependecyInjector.module

import com.alarm.alarm.Data.Retrofit.Api.AlarmaApi
import com.alarm.alarm.Data.Retrofit.Services.RetrofitModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideApiService(): AlarmaApi {
        return AlarmaApi.create()
    }

    @Provides
    fun providesRepository(): RetrofitModule{
        return RetrofitModule(provideApiService())
    }
}