package com.alarm.alarm.DaggerDependecyInjector.builder

import com.alarm.alarm.Data.Retrofit.Services.IServices
import com.alarm.alarm.Data.Retrofit.Services.RetrofitModule
import dagger.Binds
import dagger.Module


@Module
abstract class ServiceInjectors {
    @Binds
    abstract fun implApiServices(impl: RetrofitModule): IServices
}