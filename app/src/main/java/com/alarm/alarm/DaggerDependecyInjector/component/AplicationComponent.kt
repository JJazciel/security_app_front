package com.alarm.alarm.DaggerDependecyInjector.component

import com.alarm.alarm.DaggerDependecyInjector.BaseApplication
import com.alarm.alarm.DaggerDependecyInjector.module.AppModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<BaseApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<BaseApplication>()
}