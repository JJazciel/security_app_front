package com.alarm.alarm.DaggerDependecyInjector

import android.app.Application
import com.alarm.alarm.DaggerDependecyInjector.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class BaseApplication @Inject constructor() : DaggerApplication() {

    companion object {
        private var instance: Application? = null
        fun getApplication(): Application {
            return instance!!
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder().create(this)
    }
}