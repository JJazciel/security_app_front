package com.alarm.alarm.DaggerDependecyInjector.builder

import com.alarm.alarm.Activities.ConfigurationProfile.ProfileConfiguration
import com.alarm.alarm.Activities.Contact.AddContactActivity
import com.alarm.alarm.Activities.Contact.ContactActivity
import com.alarm.alarm.Activities.Enroll.LoginActivity
import com.alarm.alarm.Activities.Maps.AlertMapsActivity
import com.alarm.alarm.Activities.Menu.MenuActivity
import com.alarm.alarm.Activities.Profile.ChangeImageProfile.Change_Image_Profile_Activity
import com.alarm.alarm.Activities.Registry.RegisterActivity
import com.alarm.alarm.Activities.Splash.SplashActivity
import com.alarm.alarm.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**IMPORTANTE, SI NO REGISTRAS TU ACTIVIDAD TU APLICACION NO FUNCIONARA CORRECTAMENTE
 * Cada activity nueva que se ponga en la aplicacion tiene que ser registrada aqui:
@ContributesAndroidInjector
abstract fun bindMyActivity(): MyActivity
 * */
@Module
abstract class ActivityBuilder() {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector
    abstract fun bindMenuActivity(): MenuActivity

    @ContributesAndroidInjector
    abstract fun bindContactActivity(): ContactActivity

    @ContributesAndroidInjector
    abstract fun bindAddContactActivity(): AddContactActivity

    @ContributesAndroidInjector
    abstract fun bindProfileConfiguration():ProfileConfiguration

    @ContributesAndroidInjector
    abstract fun bindAlertMapsActivity():AlertMapsActivity

    @ContributesAndroidInjector
    abstract fun bindChangeImageProfile():Change_Image_Profile_Activity
}