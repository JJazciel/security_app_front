package com.alarm.alarm.Location

import android.app.IntentService
import android.content.Intent
import android.location.Geocoder
import java.util.*

class FetchAddressIntentService(name: String?) : IntentService(name) {
    override fun onHandleIntent(intent: Intent?) {
        val geocoder = Geocoder(this, Locale.getDefault())
        // ...
    }
}