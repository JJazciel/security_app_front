package com.alarm.alarm.Utils

import android.content.Context
import android.net.ConnectivityManager
import com.alarm.alarm.DaggerDependecyInjector.BaseApplication.Companion.getApplication
import com.alarm.alarm.Data.Retrofit.Entity.NetError
import com.alarm.alarm.R
import com.alarm.alarm.Utils.Constants.Companion.NETWORK_ERROR
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RXUtils {
    companion object {
        /**ESTA CLASE FUNCIONA PARA EL CONSUMO DE SERVICIOS CON RXJAVA NO MODIFICAR NINGUNO DE LOS METODOS
         * ESTO HABILITA QUE LOS METODOS SE EJECUTEN EN SEGUNDO PLANO, Y LAS RESPUESTAS EN EL MAIN THREAD*/

        fun <Any> applyComputationSchedulers(): ObservableTransformer<Any, Any> {
            return ObservableTransformer {
                it.subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }

        fun <Any> applyOnBackground(): ObservableTransformer<Any, Any> {
            return ObservableTransformer { upstream ->
                upstream.subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
            }
        }

        fun <Any> netWork(): ObservableTransformer<Any, Any> {
            return ObservableTransformer { upstream ->
                upstream
                    .compose(applySchedulers())
                    .compose(networkScheduler())
            }
        }

        fun <Any> netWorkBackground(): ObservableTransformer<Any, Any> {
            return ObservableTransformer { upstream ->
                upstream.compose(applyOnBackground())
                    .compose(networkScheduler())
            }
        }

        private fun <Any> applySchedulers(): ObservableTransformer<Any, Any> {
            return ObservableTransformer { upstream ->
                upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
        }

        private fun <Any> networkScheduler(): ObservableTransformer<Any, Any> {
            return ObservableTransformer { upstream ->
                upstream.doOnSubscribe {
                    if (!isNetworkEnabled) {
                        val errorNetwork =
                            NetError(
                                getApplication().getString(R.string.error_network)
                            )
                        errorNetwork.errorCode = NETWORK_ERROR
                        errorNetwork.errorMessage =
                            getApplication().getString(R.string.error_network)
                        throw errorNetwork
                    }
                }
            }
        }

        val isNetworkEnabled: Boolean
            get() {
                var connected = false
                val cm =
                    getApplication().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = cm.activeNetworkInfo
                if (activeNetwork != null) {
                    if (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                        connected = true
                    }
                }
                return connected
            }
    }
}