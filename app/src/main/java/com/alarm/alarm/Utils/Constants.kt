package com.alarm.alarm.Utils

class Constants {

    /**ES MEJOR PRACTICA UTILIZAR UN ARCHIVO DE CONSTANTES PARA ESOS VALORES QUE SE DEJAN HARDCODEADOS EN EL CODIGO
     * ¡¡¡EN UN ARCHIVO DE CONSTANTES NO SE PONEN LOS TEXTOS DE LA APP O MENSAJES DE DIALOGOS!!!!
     * EN UN ARCHIVO DE CONSTANTES VAN VARIABLES QUE SE UTILIZAN EN CODIGO, COMO LOS CODIGOS DE PAIS, KEYS DEL BUNDLE, ETC...*/

    companion object {
        const val NETWORK_ERROR = -1
        const val MAX_CONTACTS = 10
        const val COUNTRY_CODE = "52"
        const val CONTACT_LIMIT = 10
        const val NORMAL_TYPE_USER = "2"
        const val LOCATION_PERMISSION_REQUEST_CODE = 1

        const val SUCCESS_RESULT = 0
        const val FAILURE_RESULT = 1
        const val PACKAGE_NAME = "com.google.android.gms.location.sample.locationaddress"
        const val RECEIVER = "$PACKAGE_NAME.RECEIVER"
        const val RESULT_DATA_KEY = "${PACKAGE_NAME}.RESULT_DATA_KEY"
        const val LOCATION_DATA_EXTRA = "${PACKAGE_NAME}.LOCATION_DATA_EXTRA"

        const val PICK_CONTACT_REQUEST = 1
    }
}