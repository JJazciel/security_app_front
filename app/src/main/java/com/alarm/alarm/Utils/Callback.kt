package com.alarm.alarm.Utils

import android.view.View

interface CallbackItem<T> {
    fun onClickItem(view: View, item: T)
}

interface CallbackPosition {
    fun onClickItem(view: View, position: Int)
}

interface CallbackButton {
    fun onClick()
}