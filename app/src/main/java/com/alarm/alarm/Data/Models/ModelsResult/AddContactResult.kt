package com.alarm.alarm.Data.Models.ModelsResult

data class AddContactResult (
    val message: String
)