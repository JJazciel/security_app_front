package com.alarm.alarm.Data.Models.ModelsResponse

import com.alarm.alarm.Data.Models.ModelsResult.ResultLogin

data class LoginResponse(
    val result: ResultLogin,
    val response: Boolean,
    val message: String,
    val errors: String
)