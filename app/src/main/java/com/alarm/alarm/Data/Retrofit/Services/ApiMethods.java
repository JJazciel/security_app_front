package com.alarm.alarm.Data.Retrofit.Services;

import com.alarm.alarm.Data.Retrofit.Entity.ApiErrorResponse;
import com.alarm.alarm.Data.Retrofit.Entity.BaseApiErrorResponse;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import retrofit2.Response;

public class ApiMethods {

    public static <T> Function<Response<T>, Observable<T>> validate() {
        return result -> Observable.just(result)
                .flatMap(checkForRetrofitError())
                .flatMap(checkResponseForKOError())
                .flatMap(validateJSON());
    }

    /**
     * Validate returned data object.
     *
     * @param <T>
     * @return
     */
    public static <T> Function<Response<T>, ObservableSource<T>> validateJSON() {
        return result -> {
            if (result == null) {
                return Observable.error(new Throwable("ApiResponse was null"));
            }
            return Observable.just(result.body());
        };
    }

    /**
     * Check for error being returned by Retrofit.
     */
    public static <T> Function<Response<T>, ObservableSource<Response<T>>> checkForRetrofitError() {
        return result -> {
            if (!result.isSuccessful()) {
                if (result.errorBody() != null) {
                    return Observable.error(new ApiErrorResponse(result));
                }
                return Observable.error(new Throwable("Retrofit error is being present"));
            } else {
                if (result.body() != null) {
                    return Observable.just(result);
                } else {
                    if (result.errorBody() != null) {
                        return Observable.error(new Throwable(new ApiErrorResponse(result)));
                    }
                    return Observable.error(new Throwable(String.valueOf(result.code()), new Throwable("Empty Body: Sended Header Code as Message")));
                }
            }
        };
    }

    /**
     * Check if web service returned OK response but it have an error body
     *
     * @param <T> must extend BaseApiErrorResponse.
     * @return
     */
    @NotNull
    @Contract(pure = true)
    public static <T> Function<Response<T>, ObservableSource<Response<T>>> checkResponseForKOError() {
        return result -> {
            if (!(result.body() instanceof List)) {
                if (result.body() instanceof BaseApiErrorResponse) {
                    BaseApiErrorResponse res = (BaseApiErrorResponse) result.body();
                    if (res != null || res.getErrorMessage() != null) {
                        return Observable.error(res);
                    }
                }
            }
            return Observable.just(result);
        };
    }

}
