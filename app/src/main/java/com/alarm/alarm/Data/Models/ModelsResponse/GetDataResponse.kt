package com.alarm.alarm.Data.Models.ModelsResponse

import com.alarm.alarm.Data.Models.ModelsResult.getDataResult

data class GetDataResponse(
    var result: getDataResult,
    var response: Boolean,
    var message: String,
    var errors: String
)