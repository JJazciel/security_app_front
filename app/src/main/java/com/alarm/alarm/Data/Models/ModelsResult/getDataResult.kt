package com.alarm.alarm.Data.Models.ModelsResult

class getDataResult(
    var idUsuario: String,
    var Nombre: String,
    var Apellidos: String,
    var CodigoPais: String,
    var Telefono: String,
    var Correo: String,
    var ImagenUsuario: String,
    var TipoUsuario: String
)
