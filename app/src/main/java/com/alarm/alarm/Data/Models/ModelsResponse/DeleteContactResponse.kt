package com.alarm.alarm.Data.Models.ModelsResponse

import com.alarm.alarm.Data.Models.ModelsResult.getDataResult

data class DeleteContactResponse (
    var result: String,
    var response: Boolean,
    var message: String,
    var errors: String
)