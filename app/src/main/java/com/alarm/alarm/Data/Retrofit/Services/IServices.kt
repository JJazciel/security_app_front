package com.alarm.alarm.Data.Retrofit.Services

import com.alarm.alarm.Data.Models.ModelsBody.*
import com.alarm.alarm.Data.Models.ModelsResponse.*
import io.reactivex.Observable

interface IServices {
    fun login(loginBody: LoginBody): Observable<LoginResponse>
    fun registerUser(registerBody: RegisterBody): Observable<RegistryResponse>
    fun getUserData(token: String): Observable<GetDataResponse>
    fun getContacts(idUser: String): Observable<GetContactsResponse>
    fun registerDeviceToken(registerDeviceTokenBody: RegisterDeviceTokenBody): Observable<RegistryDeviceTokenResponse>
    fun issueAlert(alertBody: AlertBody): Observable<AlertResponse>
    fun addContact(contactBody: ContactBody): Observable<AddContactResponse>
    fun deleteContact(idContact: Int, idUser: Int): Observable<DeleteContactResponse>
}