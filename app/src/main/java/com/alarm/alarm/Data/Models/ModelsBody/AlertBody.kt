package com.alarm.alarm.Data.Models.ModelsBody

data class AlertBody (
    val idPersona: String,
    val TipoAlerta: Int,
    val SubtipoAlerta: Int,
    val Direccion: Direccion
)

data class Direccion(
    val Calle: String,
    val NumeroExterior: String,
    val Colonia: String,
    val Municipio: String,
    val Ciudad: String,
    val Estado: String,
    val Latitud: Double,
    val Longitud: Double,
    val TipoDireccion: Int
)