package com.alarm.alarm.Data.Models.ModelsBody

data class RegisterBody(
    val Nombre: String,
    val Apellidos: String,
    val CodigoPais: String,
    val Telefono: String,
    val Password: String,
    val TipoUsuario: String
)