package com.alarm.alarm.Data.Models.ModelsResponse

data class AlertResponse (
    var result: String,
    var response: Boolean,
    var message: String,
    var errors: String
)