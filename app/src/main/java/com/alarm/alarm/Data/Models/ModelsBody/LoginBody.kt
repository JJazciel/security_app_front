package com.alarm.alarm.Data.Models.ModelsBody

data class LoginBody(
    val CodigoPais: String,
    val Telefono: String,
    val Password: String,
    val TipoUsuario: String
)