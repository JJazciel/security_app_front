package com.alarm.alarm.Data.Preferences

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject


class Prefs @Inject constructor(val context: Context) {
    companion object {
        //Preferencias de la app
        const val APP_PREFERENCES = "appPreferences"
        const val IS_LOGIN = "login"
        const val DEVICE_TOKEN = "device_token"

        //Preferencias del usuario
        const val USER_PREFERENCES = "userPreferences"
        const val ID_USER = "id_user"
        const val NAME_USER = "name_user"
        const val LAST_NAME = "last_name_user"
        const val COUNTRY_CODE = "country_code_user"
        const val PHONE = "phone_user"
        const val EMAIL = "email_user"
        const val IMAGEPROFILE = "image_user"
        const val TYPEUSER = "type_user"
        const val TOTAL_CONTACTS = "total_contacts"
        const val REMAINING_CONTACTS = "remaining_contacts"
    }

    val prefs_app: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES, 0)

    var isLogin: Boolean
        get() = prefs_app.getBoolean(IS_LOGIN, false)
        set(value) = prefs_app.edit().putBoolean(IS_LOGIN, value).apply()

    var deviceToken: String?
        get() = prefs_app.getString(DEVICE_TOKEN, "")
        set(value) = prefs_app.edit().putString(DEVICE_TOKEN, value).apply()

    //User
    val user_prefs: SharedPreferences = context.getSharedPreferences(USER_PREFERENCES, 0)

    var idUser: String?
        get() = user_prefs.getString(ID_USER, "")
        set(value) = user_prefs.edit().putString(ID_USER, value).apply()

    var nameUser: String?
        get() = user_prefs.getString(NAME_USER, "")
        set(value) = user_prefs.edit().putString(NAME_USER, value).apply()

    var lastNameUser: String?
        get() = user_prefs.getString(LAST_NAME, "")
        set(value) = user_prefs.edit().putString(LAST_NAME, value).apply()

    var countryCodeUser: String?
        get() = user_prefs.getString(COUNTRY_CODE, "")
        set(value) = user_prefs.edit().putString(COUNTRY_CODE, value).apply()

    var phoneUser: String?
        get() = user_prefs.getString(PHONE, "")
        set(value) = user_prefs.edit().putString(PHONE, value).apply()

    var emailUser: String?
        get() = user_prefs.getString(EMAIL, "")
        set(value) = user_prefs.edit().putString(EMAIL, value).apply()

    var imageUser: String?
        get() = user_prefs.getString(IMAGEPROFILE, "")
        set(value) = user_prefs.edit().putString(IMAGEPROFILE, value).apply()

    var typeUser: String?
        get() = user_prefs.getString(TYPEUSER, "")
        set(value) = user_prefs.edit().putString(TYPEUSER, value).apply()

    var totalContacts: Int
        get() = user_prefs.getInt(TOTAL_CONTACTS, 0)
        set(value) = user_prefs.edit().putInt(TOTAL_CONTACTS, value).apply()

    var remainingContacts: Int
        get() = user_prefs.getInt(REMAINING_CONTACTS, 10)
        set(value) = user_prefs.edit().putInt(REMAINING_CONTACTS, value).apply()
}