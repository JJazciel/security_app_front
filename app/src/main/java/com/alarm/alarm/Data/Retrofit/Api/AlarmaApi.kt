package com.alarm.alarm.Data.Retrofit.Api

import com.alarm.alarm.Data.Models.ModelsBody.*
import com.alarm.alarm.Data.Models.ModelsResponse.*
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface AlarmaApi {
    @POST("auth/autenticar")
    fun login(@Body loginBody: LoginBody): Observable<Response<LoginResponse>>

    @POST("persona/registrarPersona")
    fun registerUser(@Body registerBody: RegisterBody): Observable<Response<RegistryResponse>>

    @GET("auth/getData/{tokenUser}")
    fun getUserData(@Path("tokenUser") token: String): Observable<Response<GetDataResponse>>

    @GET("contacto/listarContactos/{idUser}")
    fun getContacts(@Path("idUser") idUser: String): Observable<Response<GetContactsResponse>>

    @POST("contacto/agregarContacto")
    fun addContact(): Observable<Response<GetContactsResponse>>

    @POST("token/registrarToken")
    fun addDeviceToken(@Body registerDeviceTokenBody: RegisterDeviceTokenBody): Observable<Response<RegistryDeviceTokenResponse>>

    @POST("alerta/emitirAlerta")
    fun issueAlertUser(@Body alertBody: AlertBody): Observable<Response<AlertResponse>>

    @POST("contacto/agregarContacto")
    fun addContact(@Body contactBody: ContactBody): Observable<Response<AddContactResponse>>

    @DELETE("contacto/eliminarContacto/{idContact}/{idUser}")
    fun deleteContact(@Path("idContact") idContact: Int, @Path("idUser")idUser: Int): Observable<Response<DeleteContactResponse>>


    //TODO lo que hay dentro del companionObject es la configuracion de retrofit, ¡¡¡¡NO BORRAR!!!!
    companion object {
        //TODO(Esta variable BASE_URL deberia estar como propiedad del BuildConfig)
        const val BASE_URL = "http://dev.alerta-ciudadana.com/security_app_back/public/"

        fun create(): AlarmaApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(providesHttpClient())
                .build()
            return retrofit.create(AlarmaApi::class.java)
        }

        private fun providesHttpClient(): OkHttpClient {
            val okHttpBuilder: OkHttpClient.Builder = OkHttpClient().newBuilder()

            val httpLoggerInterception = HttpLoggingInterceptor()
            httpLoggerInterception.level = HttpLoggingInterceptor.Level.BASIC
            httpLoggerInterception.level = HttpLoggingInterceptor.Level.HEADERS
            httpLoggerInterception.level = HttpLoggingInterceptor.Level.BODY
            okHttpBuilder.addInterceptor(httpLoggerInterception)


            val interceptorHeader = Interceptor { chain: Interceptor.Chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                    .build()
                return@Interceptor chain.proceed(request)
            }

            okHttpBuilder.addInterceptor(interceptorHeader)
            okHttpBuilder.connectTimeout(1, TimeUnit.MINUTES)
            okHttpBuilder.readTimeout(45, TimeUnit.SECONDS)
            okHttpBuilder.writeTimeout(50, TimeUnit.SECONDS)

            return okHttpBuilder.build()
        }
    }
}