package com.alarm.alarm.Data.Retrofit.Entity

class NetError(var errorMessage: String) : Exception(errorMessage) {
    var errorCode: Int = 0
}