package com.alarm.alarm.Data.Models.ModelsBody

data class RegisterDeviceTokenBody(
    val idPersona: String,
    val Plataforma: String,
    val Token: String
)