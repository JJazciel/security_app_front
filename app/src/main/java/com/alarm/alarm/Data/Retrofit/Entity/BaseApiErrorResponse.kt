package com.alarm.alarm.Data.Retrofit.Entity

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseApiErrorResponse : Exception() {
    @SerializedName("message")
    @Expose
    var errorMessage: String? = null

    @SerializedName("errors")
    @Expose
    var errors: JsonObject? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other::class) return false
        val error = other as BaseApiErrorResponse
        return if (errorMessage != null) errorMessage.equals(error.errorMessage) else error.errorMessage == null
    }

    override fun hashCode(): Int {
        var result = errorMessage?.hashCode() ?: 0
        result = 31 * result + (errors?.hashCode() ?: 0)
        return result
    }
}