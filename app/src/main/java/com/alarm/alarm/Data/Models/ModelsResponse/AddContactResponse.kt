package com.alarm.alarm.Data.Models.ModelsResponse

import com.alarm.alarm.Data.Models.ModelsResult.ResultLogin

data class AddContactResponse (
    val result: String,
    val response: Boolean,
    val message: String,
    val errors: String
)