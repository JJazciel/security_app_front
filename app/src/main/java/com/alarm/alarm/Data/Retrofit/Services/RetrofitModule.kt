package com.alarm.alarm.Data.Retrofit.Services

import com.alarm.alarm.Data.Models.ModelsBody.*
import com.alarm.alarm.Data.Models.ModelsResponse.*
import com.alarm.alarm.Data.Retrofit.Api.AlarmaApi
import io.reactivex.Observable
import javax.inject.Inject

class RetrofitModule @Inject constructor(private val apiRetrofit: AlarmaApi) : IServices {

    override fun login(loginBody: LoginBody): Observable<LoginResponse> {
        return apiRetrofit.login(loginBody)
            .flatMap(ApiMethods.validate())
    }

    override fun registerUser(registerBody: RegisterBody): Observable<RegistryResponse> {
        return apiRetrofit.registerUser(registerBody)
            .flatMap(ApiMethods.validate())
    }

    override fun getUserData(token: String): Observable<GetDataResponse> {
        return apiRetrofit.getUserData(token)
            .flatMap(ApiMethods.validate())
    }

    override fun getContacts(idUser: String): Observable<GetContactsResponse> {
        return apiRetrofit.getContacts(idUser)
            .flatMap(ApiMethods.validate())
    }

    override fun registerDeviceToken(registerDeviceTokenBody: RegisterDeviceTokenBody): Observable<RegistryDeviceTokenResponse> {
        return apiRetrofit.addDeviceToken(registerDeviceTokenBody)
            .flatMap(ApiMethods.validate())
    }

    override fun issueAlert(alertBody: AlertBody): Observable<AlertResponse> {
        return apiRetrofit.issueAlertUser(alertBody)
            .flatMap(ApiMethods.validate())
    }

    override fun addContact(contactBody: ContactBody): Observable<AddContactResponse> {
        return apiRetrofit.addContact(contactBody)
            .flatMap(ApiMethods.validate())
    }

    override fun deleteContact(idContact: Int, idUser: Int): Observable<DeleteContactResponse> {
        return apiRetrofit.deleteContact(idContact, idUser)
            .flatMap(ApiMethods.validate())
    }
}