package com.alarm.alarm.Data.Models.ModelsBody

data class ContactBody(
    val Nombre: String,
    val CodigoPais: String,
    val Telefono: String,
    val Parentezco: String,
    val idPersona: String
)