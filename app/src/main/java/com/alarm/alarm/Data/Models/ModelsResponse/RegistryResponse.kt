package com.alarm.alarm.Data.Models.ModelsResponse

class RegistryResponse(
    var result: String,
    var response: Boolean,
    var message: String,
    var errors: String
)