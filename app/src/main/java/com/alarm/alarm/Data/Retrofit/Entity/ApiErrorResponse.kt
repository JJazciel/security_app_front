package com.alarm.alarm.Data.Retrofit.Entity


import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Response

class ApiErrorResponse(response: Response<*>) :
    Exception("Ocurrió un error inesperado, inténtalo nuevamente mas tarde.") {

    var errorCode: Int = -1
    var errorBody: String = ""
    var errorMessage: String? = null
    var isValidApiError = true
    private var errors: List<String>? = null

    init {
        errorCode = response.code()
        errorBody = response.errorBody()?.string() ?: "Empty Error"

        try {
            val baseApiError = Gson().fromJson(errorBody, BaseApiErrorResponse::class.java)
            baseApiError.errorMessage?.let {
                errorMessage = it
            }

            baseApiError?.errors?.let {
                errors = parseErrors(it)
            }

            errors?.let {
                if (it.isNotEmpty()) {
                    isValidApiError = true
                    errorMessage = it.first()
                }
            }
        } catch (e: Exception) {
            initCause(e)
            isValidApiError = false
            Log.e("ErrorBodyParser", response.errorBody().toString())
        }
    }

    private fun parseErrors(errors: JsonObject): MutableList<String> {
        val iteratorObj: Set<String> = errors.keySet()
        val values: MutableList<String> = mutableListOf()
        iteratorObj.forEach {
            val error = getFirstErrorArray(errors.getAsJsonArray(it))
            error?.let { message ->
                values.add(message)
            }
        }
        return values
    }

    private fun getFirstErrorArray(jsonArray: JsonArray): String? {
        val error = jsonArray.firstOrNull()
        error?.let {
            return it.asString
        }
        return null
    }

}