package com.alarm.alarm.Data.Models.ModelsResponse

import com.alarm.alarm.Data.Models.ModelsResult.GetContactsResult

data class GetContactsResponse(
    var result: List<GetContactsResult>,
    var response: Boolean,
    var message: String,
    var errors: String
)