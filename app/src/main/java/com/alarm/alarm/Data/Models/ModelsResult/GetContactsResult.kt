package com.alarm.alarm.Data.Models.ModelsResult

data class GetContactsResult(
    val idContacto: Int,
    val Nombre: String,
    val Telefono: String,
    val Parentezco: String,
    val ImagenPerfil: String
)