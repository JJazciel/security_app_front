package com.alarm.alarm.Data.Models.ModelsResponse

data class RegistryDeviceTokenResponse (
    var result: String,
    var response: Boolean,
    var message: String,
    var errors: String
)