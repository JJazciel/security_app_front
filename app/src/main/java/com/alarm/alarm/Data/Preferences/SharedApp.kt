package com.alarm.alarm.Data.Preferences

import android.app.Application

val preferencesLogin: Prefs by lazy { SharedApp.loginPrefs!! }
class SharedApp : Application() {
    companion object {
        //lateinit var loginPrefs: Prefs
        var loginPrefs: Prefs? = null
    }

    override fun onCreate() {
        super.onCreate()
        loginPrefs = Prefs(applicationContext)
    }
}